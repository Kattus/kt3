import java.util.*;

public class LongStack {

    private LinkedList<Long> list;

    public static void main(String[] argum) {
    }

    LongStack() {
        list = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack longStack = new LongStack();
        longStack.list.addAll(list);
        return longStack;
    }

    public boolean stEmpty() {
        return list.isEmpty();
    }

    public void push(long a) {
        list.addLast(a);
    }

    public long pop() {
        if (!stEmpty()) {
            long x = list.getLast();
            list.removeLast();
            return x;
        }
        throw new RuntimeException("Not enough elements to remove!");
    } // pop

    public void swap () {
        if (list.size() < 2) {throw new RuntimeException("Not enough elements for " + "SWAP");}

        long first = pop();
        long second = pop();
        push(first);
        push(second);
    }
    public void rot() {
        if (list.size() < 3) {throw new RuntimeException("Not enough elements for " + "ROT");}
        long first = pop();
        long second = pop();
        long third = pop();
        push(second);
        push(first);
        push(third);
    }
    public void op(String s) {

        if (list.size() < 2) {throw new RuntimeException("Not enough elements for " + s);}
        long y = pop();
        long x = pop();

        switch (s) {
            case "+":
                push(x + y);
                break;
            case "-":
                push(x - y);
                break;
            case "/":
                push(x / y);
                break;
            case "*":
                push(x * y);
                break;
            default:
                throw new RuntimeException("Illegal symbol" + s);
        }
    }

    public long tos() {
        try {
            return list.getLast();
        } catch (NoSuchElementException e) {
            throw new RuntimeException("There are not enough numbers in the input!");
        }
    }

    @Override
    public boolean equals(Object o) {
        return list.equals(((LongStack) o).list);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            string.append(list.get(i));
        }
        return string.toString();
    }

    public static long interpret(String pol) {
        if (pol.equals("")) {throw new RuntimeException("Empty expression!");}

        String[] splitted = pol.split("[\t ]");
        LongStack longStack = new LongStack();
        for (int i = 0; i < splitted.length; i++) {
            if (splitted[i].equals("")) { continue;}
            try {
                longStack.push(Long.parseLong(splitted[i]));
            } catch (NumberFormatException e) {
                try {
                    if (splitted[i].equals("SWAP")) {longStack.swap(); continue;}
                    if (splitted[i].equals("ROT")) {longStack.rot(); continue;}
                    longStack.op(splitted[i]);
                } catch (RuntimeException x) {
                    throw new RuntimeException(String.format("%s in expression %s", x, pol));
                }
            }
        }
        if (longStack.list.size() > 1) {
            throw new RuntimeException(String.format("Too many numbers in expression '%s", pol));
        } else if (longStack.list.size() == 0) {
            throw new RuntimeException("Empty expression!");
        }
        return longStack.list.getLast();
    }
}